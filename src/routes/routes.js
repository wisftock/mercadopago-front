import React from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import Ecommerce from '../components/Ecommerce';
import Navbar from '../components/Navbar';
import Pending from '../components/Pending';
import Producto from '../components/Producto';
import Success from '../components/Success';
import Failure from '../components/Failure';

const Rutas = () => {
  return (
    <div>
      <Navbar />
      <Routes>
        <Route path='/' element={<Ecommerce />} />
        <Route path='/producto' element={<Producto />} />
        <Route path='/success' element={<Success />} />
        <Route path='/pending' element={<Pending />} />
        <Route path='/failure' element={<Failure />} />
        {/* <Route path='/*' element={<Navigate to='/ecommerce' />} /> */}
      </Routes>
    </div>
  );
};

export default Rutas;
