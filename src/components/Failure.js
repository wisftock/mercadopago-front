import React from 'react';
import { useSearchParams } from 'react-router-dom';

const Failure = () => {
  const [params] = useSearchParams();
  const status = params.get('status');
  const external_reference = params.get('external_reference');
  const id = params.get('payment_id');
  const collection_id = params.get('collection_id');
  const preference_id = params.get('preference_id');

  return (
    <div className='container'>
      <h1>status: {status}</h1>
      <h1>external_reference: {external_reference}</h1>
      <h1>payment_id: {id}</h1>
      <h1>collection_id: {collection_id}</h1>
      <h1>preference_id: {preference_id}</h1>
    </div>
  );
};

export default Failure;
