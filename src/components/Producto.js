import React, { useEffect, useState } from 'react';

const Producto = () => {
  const [listPreferencia, setListPreferencia] = useState([]);

  const handleClick = async () => {
    const url = 'https://mercadopago-back.herokuapp.com/create_preference';
    await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((data) => data.json())
      .then((data) => setListPreferencia(data));
  };
  useEffect(() => {
    handleClick();
  }, []);

  const handleScriptLoad = () => {
    const mp = new window.MercadoPago('APP_USR-72b18fd4-a674-424c-9b9d-edd7f5f29814', {
      locale: 'es-PE',
    });
    mp.checkout({
      preference: {
        id: `${listPreferencia.id}`,
      },
    });
  };

  const handlePagar = () => {
    let script = document.createElement('script');
    script.type = 'text/javascript';

    if (script.readyState) {
      script.onreadystatechange = () => {
        if (script.readyState === 'loaded' || script.readyState === 'complete') {
          script.onreadystatechange = null;
          handleScriptLoad();
        }
      };
    } else {
      script.onload = () => handleScriptLoad();
    }
    script.src = 'https://sdk.mercadopago.com/js/v2';
    document.getElementsByTagName('head')[0].appendChild(script);
  };

  return (
    <div className='my-5 mx-5'>
      <div className='card' style={{ width: '28rem' }}>
        <img src='...' className='card-img-top' alt='...' />
        <div className='card-body'>
          <h5 className='card-title'>Card title</h5>
          <p className='card-text'>
            Some quick example text to build on the card title and make up the bulk of the card's
            content.
          </p>

          <a
            href={listPreferencia?.response?.response?.init_point}
            className='payment-form btn btn-primary'
            id='payment-form'
            onClick={() => handlePagar()}
          >
            Pagar
          </a>
        </div>
      </div>
    </div>
  );
};

export default Producto;
