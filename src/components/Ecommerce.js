import React from 'react';
import { useNavigate } from 'react-router-dom';
import { database } from '../database/database';
import Menu from './Menu';

const Ecommerce = () => {
  const navigate = useNavigate();
  const handleClick = (e) => {
    navigate('/producto');
  };
  return (
    <>
      <div className='container-fluid'>
        <div className='row mt-4'>
          <div className='col-3'>
            <Menu />
          </div>
          <div className='col-9 row justify-content-between' style={{ width: '100%' }}>
            {database.map((item, index) => {
              return (
                <div key={index} className='card mb-4' style={{ width: '14rem', height: '310px' }}>
                  <img
                    src={item.picture_url}
                    className='card-img-top'
                    alt={item.title}
                    style={{ width: 'auto', height: '65%' }}
                  />
                  <div className='card-body'>
                    <div>
                      <h5 className='card-title d-flex justify-content-between'>
                        {item.title} <span>${item.unit_price}</span>
                      </h5>
                    </div>
                    <button
                      className='btn btn-primary'
                      style={{ width: '100%' }}
                      onClick={() => handleClick(item)}
                    >
                      Agregar
                    </button>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </>
  );
};

export default Ecommerce;
