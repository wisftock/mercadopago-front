import React from 'react';

const tragos = [
  {
    title: 'Coctel',
  },
  {
    title: 'Vinos',
  },
  {
    title: 'Cerveza',
  },
  {
    title: 'Espumante',
  },
  {
    title: 'Ron',
  },
  {
    title: 'wisky',
  },
  {
    title: 'Champan',
  },
  {
    title: 'Agua',
  },
  {
    title: 'Gaseosa',
  },
];

const Menu = () => {
  return (
    <div className='card'>
      <div className='card-body'>
        <input type='text' className='form-control' />
        <div>
          <ul className='mt-4'>
            {tragos.map((item, index) => {
              return (
                <li
                  key={index}
                  className={index === 0 ? ' bg-success text-white' : ''}
                  style={{
                    listStyle: 'none',
                    backgroundColor: '#f4f4f4',
                    width: '100% !important',
                    margin: '6px 0px',
                    padding: '6px 0px',
                    textAlign: 'center',
                    cursor: 'pointer',
                  }}
                >
                  {item.title}
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Menu;
