export const database = [
  {
    id: '1234',
    title: 'coctel 1',
    description: `There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.`,
    unit_price: '20.00',
    quantity: '4',
    picture_url:
      'https://i.pinimg.com/236x/21/47/75/2147754d58ea31859eed05c26b753527.jpg',
  },
  {
    id: '1235',
    title: 'coctel 1',
    description: `There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.`,
    unit_price: '28.72',
    quantity: '4',
    picture_url:
      'https://www.demoslavueltaaldia.com/sites/default/files/coctel-anana-sidra.jpg',
  },
  {
    id: '1236',
    title: 'coctel 1',
    description: `There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.`,
    unit_price: '12.32',
    quantity: '4',
    picture_url:
      'https://www.thegourmetjournal.com/wp-content/uploads/2012/11/Metropole-C%C3%B3cteles.jpg',
  },
  {
    id: '1237',
    title: 'coctel 1',
    description: `There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.`,
    unit_price: '87.80',
    quantity: '4',
    picture_url:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQODD7z58D7WLOk7ywzdoKsy1aHpTOjLdbpk-lAJDKDllZB6h5tSlQ1OgiSrNxYY0cVPS4&usqp=CAU',
  },
  {
    id: '1238',
    title: 'coctel 1',
    description: `There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.`,
    unit_price: '43.76',
    quantity: '4',
    picture_url:
      'https://www.losvinos.com.ar/wp-content/uploads/2019/06/coctel-blue-ocean-ginebra.jpg?ezimgfmt=rs:382x569/rscb13/ng:webp/ngcb13',
  },
  {
    id: '1239',
    title: 'coctel 1',
    description: `There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.`,
    unit_price: '150.00',
    quantity: '4',
    picture_url:
      'https://i.pinimg.com/736x/2c/71/90/2c7190f8ff800f0494ad11bd2e1c7114--cocktail-change-.jpg',
  },
  {
    id: '1230',
    title: 'coctel 1',
    description: `There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.`,
    unit_price: '23.70',
    quantity: '4',
    picture_url:
      'https://us.123rf.com/450wm/haveseen/haveseen1106/haveseen110600057/9828745-c%C3%B3ctel-de-tequila-sunrise-aislado-en-blanco.jpg?ver=6',
  },
  {
    id: '1276',
    title: 'coctel 1',
    description: `There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.`,
    unit_price: '50.00',
    quantity: '4',
    picture_url:
      'https://i.pinimg.com/236x/b5/50/3e/b5503e9572f2ee2b18fb6fb3998209aa--curacao-azul-ron-blanco.jpg',
  },
];
